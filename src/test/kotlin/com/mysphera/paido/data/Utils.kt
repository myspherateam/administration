package com.mysphera.paido.data

import org.junit.jupiter.api.Assertions.assertTrue
import java.util.function.Supplier

fun <T> assertHasSameElements(expected: Collection<T> , actual: Collection<T> , messageSupplier: Supplier<String>? = null){
    assertTrue(expected.size == actual.size && expected.containsAll(actual) && actual.containsAll(expected), messageSupplier)
}