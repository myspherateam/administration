package com.mysphera.paido.data

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class RolTest {

    @Test
    fun `valueOf return expected role`() {
        for(role in Rol.values()){
            assertEquals(role, Rol.fromString(role.toString())) {"for role $role"}
        }
    }

    @Test
    fun `joining single role returns the list for the role`() {
        for(role in Rol.values()){
            assertEquals(role.manages, Rol.manages(listOf(role.toString()))){"for role $role"}
        }
    }

    @Test
    fun `joining some returns the expected list`() {
        assertThat(Rol.manages(listOf(Rol.TEACHER.toString(), Rol.LOCAL_ADMIN.toString())))
            .containsOnlyOnceElementsOf(listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.CHILD, Rol.PARENT))
    }

    @Test
    fun `does not duplicate roles`() {
        assertThat(Rol.manages(listOf(Rol.TEACHER.toString(), Rol.CLINICIAN.toString())))
            .containsOnlyOnceElementsOf(listOf(Rol.CHILD, Rol.PARENT))
    }
}
