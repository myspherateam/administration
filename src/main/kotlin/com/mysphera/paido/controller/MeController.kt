package com.mysphera.paido.controller

import com.mysphera.paido.DataException
import com.mysphera.paido.Response
import com.mysphera.paido.data.Ranking
import com.mysphera.paido.data.RankingType
import com.mysphera.paido.data.Rol
import com.mysphera.paido.data.User
import com.mysphera.paido.getJWTPayload
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/me")
class MeController : AbstractController() {

    @GetMapping
    fun me(request: HttpServletRequest): Response<User> {
        val caller = request.getJWTPayload().id
        val callerUser = userRepo.findById(caller).orElseThrow {
            throw DataException("Data error with user: $caller")
        }

        if (callerUser.hasRol(Rol.CHILD)) {
            val parentsIds = callerUser.parents ?: emptyList()
            callerUser.parentsUsers = userRepo.findAllById(parentsIds).toList()
        }

        if (callerUser.hasRol(Rol.PARENT)) {
            val childrenIds = callerUser.caringForIds ?: emptyList()
            callerUser.childsUsers = userRepo.findAllById(childrenIds).toList()
        }
        return Response(callerUser)
    }

    @GetMapping("/mobile")
    fun mobile(request: HttpServletRequest): Response<List<User>> {
        val caller = request.getJWTPayload().id
        val objectIds = userRepo.findById(caller).orElseThrow {
            throw DataException("Data error with user: $caller")
        }.caringForIds?.plus(caller) ?: listOf(caller)

        val result = userRepo.findAllById(objectIds).toList()
        return Response(result, count = result.size.toLong())
    }

    @GetMapping("/ranking")
    fun ranking(request: HttpServletRequest,
                @RequestParam(required = true) field: String,
                @RequestParam(required = true) rankingType: RankingType
                ): Response<Ranking>{
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.CHILD))
        val callerId = callerUser.id ?: throw DataException("Data error with user: $callerUser")
        val entityIds = if(rankingType == RankingType.entity) callerUser.entityIds else null
        val groupIds = if(rankingType == RankingType.group) callerUser.groupIds else null

        return Response(userRepo.getRanking(callerId, field, entityIds, groupIds))
    }
}
