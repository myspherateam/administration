package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.Group
import com.mysphera.paido.data.RankingType
import com.mysphera.paido.data.Rol
import com.mysphera.paido.data.User
import com.mysphera.paido.utils.KeycloakHelper
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Schema
import org.apache.commons.lang3.time.DateUtils
import org.bson.types.ObjectId
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import kotlin.concurrent.thread

@Suppress("unused")
@RestController
@RequestMapping("/users")
class UsersController : AbstractController() {
    @Value("\${credentials.seconds-to-check}")
    private lateinit var credentialsSecondsToCheck: String

    @Value("\${credentials.expires}")
    private lateinit var credentialsExpires: String

    @Value("\${microservice.keycloak.paido.realm}")
    private lateinit var keycloakPAIDORealm: String

    @Value("\${microservice.keycloak.mailing-enabled}")
    private lateinit var keycloakMailingEnabled: String

    @Value("\${registration-code.days-to-expire-on-user-creation}")
    private lateinit var regCodeDaysToExpireOnUserCreation: String

    @Value("\${registration-code.days-to-expire-on-reg-code-regeneration}")
    private lateinit var regCodeDaysToExpireOnRegCodeRegen: String

    @Autowired
    protected lateinit var keycloakHelper: KeycloakHelper

    /**
     * Returns a list of users based on the input filters
     */
    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) page: Int?,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) roles: List<String>?,
        @RequestParam(required = false) ids: List<ObjectId>?,
        @RequestParam(required = false) groupIds: List<ObjectId>?,
        @RequestParam(required = false) entityIds: List<ObjectId>?
    ): Response<List<User>> {
        val callerUser = denyIfNotValidRoles(
            request,
            listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN, Rol.GLOBAL_ADMIN)
        )

        val isTeacher = callerUser.hasRol(Rol.TEACHER)
        val isClinician = callerUser.hasRol(Rol.CLINICIAN)
        val isLocalAdmin = callerUser.hasRol(Rol.LOCAL_ADMIN)
        val isGlobalAdmin = callerUser.hasRol(Rol.GLOBAL_ADMIN)

        val providedEntityIds = entityIds ?: emptyList()
        val callerEntityIds = callerUser.entityIds ?: emptyList()

        val result = when {
            //We'll always check first if challenges backend is calling us
            //if so, just retrieve every result found, challenges will filter results by itself
            userAgentIs(request.getHeader(HttpHeaders.USER_AGENT), "challenges") -> {
                userRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    ids = ids,
                    groupIds = groupIds,
                    entityIds = entityIds,
                    roles = roles
                )
            }
            !isGlobalAdmin && entityIds.isNullOrEmpty() -> throw BadRequestException("entityIds is required")
            isGlobalAdmin -> {
                val allowedRoles = Rol.GLOBAL_ADMIN.manages.map { it.type }
                userRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    roles = roles ?: allowedRoles
                )
            }
            isLocalAdmin -> {
                val askForNonRelatedEntity = providedEntityIds.any { entity ->
                    !callerEntityIds.contains(entity)
                }
                if (askForNonRelatedEntity) {
                    throw ForbiddenException("The user cant read users info of non related entity")
                }
                val allowedRoles = Rol.LOCAL_ADMIN.manages.map { it.type }
                userRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    entityIds = entityIds,
                    roles = allowedRoles
                )
            }
            isTeacher || isClinician -> {
                val askForNonRelatedEntity = providedEntityIds.any { entity ->
                    !callerEntityIds.contains(entity)
                }
                if (askForNonRelatedEntity) {
                    throw ForbiddenException("The user cant read users info of non related entity")
                }
                val teacherClinicianRoles = Rol.TEACHER.manages.map { it.type }
                val authorityRoles = mutableListOf(Rol.CLINICIAN.type, Rol.TEACHER.type)
                val defaultRoles = teacherClinicianRoles + authorityRoles
                val queryRoles = roles?.let {
                    if (!defaultRoles.containsAll(it)) {
                        throw ForbiddenException(
                            "User is forbidden to asking for other roles than: ${
                                defaultRoles.joinToString(
                                    ","
                                )
                            }"
                        )
                    }
                    it
                } ?: defaultRoles

                val creator =
                    if (!queryRoles.contains(Rol.TEACHER.type) && !queryRoles.contains(Rol.CLINICIAN.type)) {
                        callerUser.resolveUserId()
                    } else {
                        null
                    }

                userRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    ids,
                    groupIds,
                    entityIds,
                    creator,
                    queryRoles
                )
            }
            else -> throw ForbiddenException("The user cant read")
        }

        result.data.map {
            when {
                it.roles?.contains("Child") == true -> {
                    it.parentsUsers = userRepo.findAllById(it.parents ?: emptyList()).toList()
                }
                it.roles?.contains("Parent") == true -> {
                    it.childsUsers = userRepo.findAllById(it.caringForIds ?: emptyList()).toList()
                }
            }
        }

        return Response(result.data, count = result.total)
    }

    @PostMapping("/lostPassword")
    fun sendNewRegistrationEmail(
        @RequestBody user: User
    ): Response<Boolean> {
        if (!keycloakMailingEnabled.toBoolean()) {
            throw ForbiddenException("mailing is disabled")
        }

        val emailProvided = user.email ?: throw BadRequestException("No email was provided")

        return Response(sendUpdatePasswordEmailTo(emailProvided))
    }

    @PostMapping("/{id}/resendEmail")
    fun sendNewRegistrationEmail(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<Boolean> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN))

        if (!keycloakMailingEnabled.toBoolean()) {
            throw ForbiddenException("mailing is disabled")
        }

        val storedUser = userRepo.findById(id)
            .orElseThrow { throw BadRequestException("id provided was not found.") }

        if (storedUser.hasRol(Rol.CHILD) || storedUser.hasRol(Rol.PARENT) || storedUser.hasRol(Rol.GLOBAL_ADMIN)) {
            throw BadRequestException("Can't resend email for Child, Parent or Global Admin")
        }

        val emailToResend =
            storedUser.email ?: throw DataException("User ${storedUser.id} has no email")

        return Response(sendUpdatePasswordEmailTo(emailToResend))
    }

    @GetMapping("/{id}/registrationCode")
    fun getNewRegistrationCode(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<User> {
        denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val storedUser = userRepo.findById(id)
            .orElseThrow { throw BadRequestException("id provided was not found.") }
            .apply {
                registrationCodeExpiresAt =
                    DateUtils.addDays(Date(), regCodeDaysToExpireOnRegCodeRegen.toInt())
                registrationCode = if (userRegistrationCode != null) {
                    userRegistrationCode
                } else {
                    FullRandomString.nextString(6).toUpperCase()
                }
            }

        return Response(userRepo.save(storedUser))
    }

    @GetMapping("/{id}")
    fun getOne(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<User> {
        val callerUser = denyIfNotValidRoles(
            request,
            listOf(
                Rol.TEACHER,
                Rol.CLINICIAN,
                Rol.GLOBAL_ADMIN,
                Rol.LOCAL_ADMIN,
                Rol.CHILD,
                Rol.PARENT
            )
        )
        val caller = callerUser.resolveUserId()

        val isGlobalAdmin = callerUser.hasRol(Rol.GLOBAL_ADMIN)
        val isLocalAdmin = callerUser.hasRol(Rol.LOCAL_ADMIN)
        val isChild = callerUser.hasRol(Rol.CHILD)
        val isParent = callerUser.hasRol(Rol.PARENT)
        val isTeacherOrClinician =
            callerUser.hasRol(Rol.TEACHER) || callerUser.hasRol(Rol.CLINICIAN)

        val result =
            userRepo.findById(id).orElseThrow { throw NotFoundException("No user was found") }

        val resultEntities = result.entityIds ?: emptyList()
        val resultGroups = result.groupIds ?: emptyList()
        val callerEntities = callerUser.entityIds ?: emptyList()
        val callerGroups = callerUser.groupIds ?: emptyList()
        val hasAnyCommonEntity = resultEntities.any { callerEntities.contains(it) }
        val hasAnyCommonGroup = resultGroups.any { callerGroups.contains(it) }

        when {
            isParent && id != caller && !callerUser.isParentOf(id) -> {
                throw ForbiddenException("Parent is allowed to read itself or children")
            }
            isChild && id != caller -> {
                throw ForbiddenException("Child is allowed to read itself")
            }
            isGlobalAdmin && id != caller && !result.hasRol(Rol.LOCAL_ADMIN) -> {
                throw ForbiddenException("Global Admin is allowed to read itself or Local Admin")
            }
            isLocalAdmin && id != caller && !result.hasRol(Rol.TEACHER) && !result.hasRol(Rol.CLINICIAN) -> {
                throw ForbiddenException("Local Admin is allowed to read itself or Teacher/Clinician")
            }
            isTeacherOrClinician && id != caller && !result.hasRol(Rol.CHILD) && !result.hasRol(Rol.PARENT)
                    && !result.hasRol(Rol.CLINICIAN) && !result.hasRol(Rol.TEACHER) -> {
                throw ForbiddenException("Teacher/Clinician is allowed to read itself or Child/Parent/Clinician/Teacher")
            }
            isTeacherOrClinician && id != caller && !hasAnyCommonEntity && !hasAnyCommonGroup -> {
                throw ForbiddenException("Teacher/Clinician is not allowed to read non-related User")
            }
        }

        if (result.hasRol(Rol.CHILD)) {
            val parentsIds = result.parents ?: emptyList()
            result.parentsUsers = userRepo.findAllById(parentsIds).toList()
        }

        if (result.hasRol(Rol.PARENT)) {
            val childrenIds = result.caringForIds ?: emptyList()
            result.childsUsers = userRepo.findAllById(childrenIds).toList()
        }

        return Response(result)
    }

    @GetMapping("/{id}/stats")
    fun getStatistics(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId,
        @RequestParam(required = true) field: String,
        @RequestParam(required = true) rankingType: RankingType
    ): Response<User> {

        val getOneResponse = getOne(request, id)

        val user = getOneResponse.message
        val callerId = user.id ?: throw DataException("Data error with user: $user")
        if (user.roles?.contains(Rol.CHILD.type) != true) throw DataException("Can only be called for children")
        val entityIds = if (rankingType != RankingType.global) user.entityIds else null
        val groupIds = if (rankingType != RankingType.global) user.groupIds else null

        try {
            user.ranking = userRepo.getRanking(callerId, field, entityIds, groupIds)
        } catch (e: BadRequestException) {
            //Cant get ranking, simply return the previous data
        }


        return getOneResponse
    }

    private fun checkCanManageUser(callerUser: User, user: User) {
        if (callerUser.id == user.id) {
            return
        }

        val rolesAllowed = Rol.manages(callerUser.roles)

        val userRoles = Rol.toListOfRoles(user.roles)
        if (userRoles.isEmpty()) {
            throw BadRequestException(
                "No roles were provided (Example: ${
                    rolesAllowed.joinToString(
                        ", "
                    )
                })"
            )
        }
        val notValidRoles = userRoles.filterNot { rolesAllowed.contains(it) }
        if (notValidRoles.isNotEmpty()) {
            throw BadRequestException(
                "Provided roles '${notValidRoles.joinToString(", ")}' are not valid. (Valid roles: ${
                    rolesAllowed.joinToString(", ")
                })"
            )
        }
        val userEntityIds = user.entityIds
        if (!userEntityIds.isNullOrEmpty() &&
            !callerUser.hasRol(Rol.GLOBAL_ADMIN) &&
            callerUser.entityIds?.containsAll(userEntityIds) != true
        ) {
            throw BadRequestException(
                "Must have the same entityIds than target user (${
                    userEntityIds.filterNot { callerUser.entityIds?.contains(it) == true }
                        .joinToString(", ")
                }) or be a Global Admin"
            )

        }
    }

    private fun sanitizeRoles(user: User) {
        val userRoles = user.roles
        if (userRoles != null) {
            if (userRoles.contains(Rol.CHILD.type) && (userRoles.size > 1)) {
                throw BadRequestException("User with Child role. A child cant be another role too")
            }
            if (userRoles.contains(Rol.PARENT.type) && userRoles.size > 1) {
                throw BadRequestException("User with Parent role. A parent cant be another role too")
            }
            user.roles = if (user.hasRol(Rol.LOCAL_ADMIN)) {
                userRoles.plus(listOf(Rol.TEACHER.type, Rol.CLINICIAN.type))
            } else {
                userRoles
            }
        }
    }

    private fun create(callerUser: User, user: User): User {
        with(user) {
            creator = callerUser.id ?: throw DataException("User has no id.")

            checkCanManageUser(callerUser, user)
            sanitizeRoles(user)

            displayId = countersRepo.incrementAndGetUserDisplayId()

            val isCreatingLocalAdmin = user.hasRol(Rol.LOCAL_ADMIN)
            val isCreatingParent = user.hasRol(Rol.PARENT)
            val isCreatingChild = user.hasRol(Rol.CHILD)

            if (!isCreatingLocalAdmin) {
                entityIds = validateEntities(entityIds)
            }
            // TODO update challenges
            // TODO update keycloak data
            if (isCreatingChild) {
                points = 0
                val newParents = user.parents
                if (newParents != null) {
                    validateUsersOfRole(newParents, Rol.PARENT)
                    user.parents = newParents
                }
                val newGroups = user.groupIds
                if (newGroups != null) {
                    validateGroups(newGroups, user.entityIds)
                    user.groupIds = newGroups
                }
            }

            if (isCreatingParent) {
                caringForIds = validateCaringForIds(caringForIds)
                val newChildren = user.caringForIds
                if (newChildren != null) {
                    validateUsersOfRole(newChildren, Rol.CHILD)
                    user.caringForIds = newChildren
                }
            }

            if (!isCreatingChild && !isCreatingParent) {
                email?.let {
                    val emailRegex = """(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})""".toRegex()
                    if (!emailRegex.containsMatchIn(it)) {
                        throw BadRequestException("Invalid email '$it'")
                    }
                } ?: throw BadRequestException("email is required")
            }

            val sumDaysToExpire = regCodeDaysToExpireOnUserCreation.toInt()
            val expiresAt = DateUtils.addDays(Date(), sumDaysToExpire)
            registrationCode = FullRandomString.nextString(6).toUpperCase()
                .takeIf { isCreatingParent || isCreatingChild }
            registrationCodeExpiresAt = expiresAt
                .takeIf { isCreatingParent || isCreatingChild }

            clean(user)
        }

        val createdUser = userRepo.save(user)
        val createdId = createdUser.id

        if (createdId != null) {
            if (user.hasRol(Rol.CHILD)) {
                val newParents = user.parents
                if (newParents != null) {
                    addAndRemoveChild(listOf(), newParents, createdId)
                }
            }

            if (user.hasRol(Rol.PARENT)) {
                val newChildren = user.caringForIds
                if (newChildren != null) {
                    addAndRemoveParent(listOf(), newChildren, createdId)
                }
            }
        }
        updateKeycloakAttributes(createdUser)

        return createdUser
    }

    //TODO iarjona: Not supporting update groupsIds / entityIds yet. We should recalculate challenges.
    /**
     * Supports updating Child/Parent, it must be invoked by Teacher/Clinician user
     * LocalAdmin, GlobalAdmin, Teacher, Clinician can't be updated.
     * email CAN'T be updated, without affect OAuth SV
     * displayName is updatable ONLY by using POST /users/mobile
     */
    private fun update(
        callerUser: User,
        id: ObjectId,
        temp: User,
        isChallenges: Boolean = false
    ): User {
        val stored = userRepo.findById(id).orElseThrow {
            throw BadRequestException("id provided was not found")
        }
        if (!isChallenges) {
            checkCanManageUser(callerUser, stored)
        }
        sanitizeRoles(stored)


//        val isUpdatingChild = hasRol(Rol.CHILD)
//        val isUpdatingParent = hasRol(Rol.PARENT)
//
//        if (!isUpdatingChild && !isUpdatingParent) {
//            throw ForbiddenException("We can update Child/Parent only")
//        }

        var changedGroupsOrParentsOfChild = false
        var changedCaringForOfParent = false


        // TODO update challenges
        // TODO update keycloak data
        // TODO check group creator
        if (stored.hasRol(Rol.CHILD)) {
            stored.externalId = temp.externalId ?: stored.externalId
            stored.potentialPoints = temp.potentialPoints ?: stored.potentialPoints ?: 0
            stored.points = temp.points ?: stored.points ?: 0
            stored.steps = temp.steps ?: stored.steps
            stored.numAssignedChallenges =
                temp.numAssignedChallenges ?: stored.numAssignedChallenges
            stored.numCompletedChallenges =
                temp.numCompletedChallenges ?: stored.numCompletedChallenges
            stored.badges = temp.badges ?: stored.badges
            stored.futureBadges = temp.futureBadges ?: stored.futureBadges
            val newParents = temp.parents
            if (newParents != null) {
                validateUsersOfRole(newParents, Rol.PARENT)
                addAndRemoveChild(stored.parents ?: listOf(), newParents, id)
                stored.parents = newParents
                changedGroupsOrParentsOfChild = true
            }

            val newGroups = temp.groupIds
            if (newGroups != null) {
                validateGroups(newGroups, stored.entityIds)
                stored.groupIds = newGroups
                changedGroupsOrParentsOfChild = true
            }

//      entityIds = validateEntities(temp.entityIds)
//      groupIds = validateGroups(temp.groupIds)
        }
        if (stored.hasRol(Rol.PARENT)) {
            val newChildren = temp.caringForIds
            if (newChildren != null) {
                validateUsersOfRole(newChildren, Rol.CHILD)
                addAndRemoveParent(stored.caringForIds ?: listOf(), newChildren, id)
                stored.caringForIds = newChildren
                changedCaringForOfParent = true
            }

            stored.potentialPoints = temp.potentialPoints ?: stored.potentialPoints ?: 0
            stored.points = temp.points ?: stored.points ?: 0
            stored.steps = temp.steps ?: stored.steps
            stored.numAssignedChallenges =
                temp.numAssignedChallenges ?: stored.numAssignedChallenges
            stored.numCompletedChallenges =
                temp.numCompletedChallenges ?: stored.numCompletedChallenges
        }

        clean(stored)

        val updatedUser = userRepo.save(stored)

        updateKeycloakAttributes(updatedUser)

        if (changedGroupsOrParentsOfChild) {
            stored.parents?.let { updateParentGroups(it) }
        }
        if (changedCaringForOfParent) {
            updatedUser.id?.let { updateParentGroups(listOf(it)) }
        }

        return updatedUser
    }

    /*
        When editing children:
        Adding a parent:
            Change the parent's caringForIds
        Removing a parent:
            Change the parent's caringForIds
        Moving group:
            Adding challenges from the new group
            Edit all the parent's groups to match

        When editing parents:
        Adding a child:
            Change the child's parents
        Removing a child:
            Change the child's parents

     */

    /**
     * Removes unused data according to user rol
     */
    private fun clean(user: User) {
        with(user) {
            val isChild = hasRol(Rol.CHILD)
            val isParent = hasRol(Rol.PARENT)

            externalId = externalId.takeIf { isChild }
            email = email.takeIf { !isChild && !isParent }
            points = points.takeIf { isChild }
            groupIds = groupIds.takeIf { isChild || isParent }
            parents = parents.takeIf { isChild }
            caringForIds = caringForIds.takeIf { isParent }
        }
    }

    private fun validateUsersOfRole(userIds: List<ObjectId>, role: Rol): List<User> {
        val users = userRepo.findAllById(userIds).toList()
        if (users.size != userIds.size) {
            throw BadRequestException(
                "Some of the $role provided were not found (provided: ${
                    userIds.joinToString(
                        ", "
                    )
                }, found: ${users.joinToString(", ") { it.id.toString() }})"
            )
        }
        val nonRoleFound = users.filter {
            !it.hasRol(role)
        }

        if (nonRoleFound.isNotEmpty()) {
            throw BadRequestException(
                "Some of the users provided did not have the $role role (${
                    nonRoleFound.joinToString(
                        "; "
                    ) { "${it.id}-[${it.roles?.joinToString(", ")}]" }
                })"
            )
        }
        return users
    }

    private fun addAndRemoveChild(
        oldParents: List<ObjectId>,
        newParents: List<ObjectId>,
        childId: ObjectId
    ) {
        val parentsToAdd = newParents.filter { newPId -> oldParents.none { it == newPId } }
        val parentsToRemove = oldParents.filter { oldPId -> newParents.none { it == oldPId } }
        parentsToAdd.forEach { userRepo.addChild(it, childId); updateKeycloakAttributesFromId(it) }
        parentsToRemove.forEach {
            userRepo.removeChild(it, childId); updateKeycloakAttributesFromId(
            it
        )
        }
        updateParentGroups(parentsToAdd.plus(parentsToRemove))
    }

    private fun addAndRemoveParent(
        oldChildren: List<ObjectId>,
        newChildren: List<ObjectId>,
        parentId: ObjectId
    ) {
        val childrenToAdd = newChildren.filter { newCId -> oldChildren.none { it == newCId } }
        val childrenToRemove = oldChildren.filter { oldCId -> newChildren.none { it == oldCId } }
        childrenToAdd.forEach {
            userRepo.addParent(
                it,
                parentId
            ); updateKeycloakAttributesFromId(it)
        }
        childrenToRemove.forEach {
            userRepo.removeParent(
                it,
                parentId
            ); updateKeycloakAttributesFromId(it)
        }
    }

    private fun validateGroups(groupIds: List<ObjectId>, entityIds: List<ObjectId>?): List<Group> {
        val groups = groupRepo.findAllById(groupIds).toList()
        if (groups.size != groupIds.size) {
            throw BadRequestException(
                "Some of the groups provided were not found (provided: ${
                    groupIds.joinToString(
                        ", "
                    )
                }, found: ${groups.joinToString(", ") { it.id.toString() }})"
            )
        }
        val nonMatchingFound = groups.filterNot {
            entityIds?.contains(it.entityId) == true
        }

        if (nonMatchingFound.isNotEmpty()) {
            throw BadRequestException(
                "Some of the groups provided did not have the same entityIds as the user (${
                    nonMatchingFound.joinToString(
                        "; "
                    ) { "${it.id}-${it.entityId}" }
                })"
            )
        }
        return groups

    }

    private fun updateParentGroups(parents: List<ObjectId>) {
        userRepo
            .findAllById(parents)
            .forEach { parent ->
                parent.groupIds = parent.caringForIds?.let { childIds ->
                    userRepo
                        .findAllById(childIds)
                        .fold(setOf<ObjectId>()) { acc, child ->
                            acc.plus(
                                child.groupIds ?: listOf()
                            )
                        }
                        .toList()
                }
                userRepo.save(parent)
            }
    }

    private fun validateEntities(entityIds: List<ObjectId>?): List<ObjectId> {
        entityIds?.let { ids ->
            if (ids.isEmpty()) {
                throw BadRequestException("No entityIds were provided")
            }

            val entities = entityRepo.findAllById(ids).map {
                it.id ?: throw DataException("Entity has no id")
            }
            val notExisting = ids.filterNot { entities.contains(it) }
            if (notExisting.isNotEmpty()) {
                throw BadRequestException("Provided entityIds '${notExisting.joinToString(", ")}' doesn't exist")
            }

            return entities
        } ?: throw BadRequestException("entityIds is required")
    }

    private fun validateCaringForIds(caringForIds: List<ObjectId>?): List<ObjectId> {
        caringForIds?.let { ids ->
            if (ids.isEmpty()) {
                throw BadRequestException("No caringForIds values were provided")
            }
            if (ids.size > 6) {
                throw BadRequestException("We can't allow Parent user with more than 6 children")
            }

            val userIds = userRepo.findAllById(ids).onEach { u ->
                if (!u.hasRol(Rol.CHILD)) {
                    throw BadRequestException("Provided caringForIds user '${u.id}' is not a Child")
                }
            }.map { it.id ?: throw DataException("User has no id") }

            val notExisting = ids.subtract(userIds.toSet())
            if (notExisting.isNotEmpty()) {
                throw BadRequestException(
                    "Provided caringForIds '${notExisting.joinToString(", ")}' doesn't exist"
                )
            }

            return userIds
        } ?: throw BadRequestException("caringForIds is required")
    }

    @PostMapping("/mobile")
    fun updateMobile(request: HttpServletRequest, @RequestBody user: User): Response<User> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.CHILD, Rol.PARENT))

        // Simple registration case from the parent's device. Just update the display name
        if (callerUser.hasRol(Rol.PARENT) && user.hasRol(Rol.CHILD)) {
            val oldUser = userRepo.findById(user.id)
                .orElseThrow { NotFoundException("user with id ${user.id} not found") }
            val displayId =
                user.displayId ?: throw DataException("User has no displayId ${user.id}")
            val newDisplayName =
                user.displayName ?: throw BadRequestException("displayName is required")
            oldUser.displayName = userRepo.getFullDisplayName(newDisplayName, displayId)
            return Response(userRepo.save(oldUser))
        }
        if (callerUser.hasRol(Rol.CHILD) && callerUser.displayName == null) {
            val displayId =
                user.displayId ?: throw DataException("User has no displayId ${user.id}")
            val newDisplayName =
                user.displayName ?: throw BadRequestException("displayName is required")
            callerUser.displayName = userRepo.getFullDisplayName(newDisplayName, displayId)
        }

        val kcUsername = callerUser.resolveUserId().toString()
        val password = user.password?.apply {
            if (this.length < 8) throw BadRequestException("password must have min. 8 characters")

        } ?: throw BadRequestException("password is required")
        val isUpdatedOnKC = updateKeycloakPasswordForUser(kcUsername, password)

        if (isUpdatedOnKC) {
            return Response(userRepo.save(callerUser))
        } else {
            throw AuthServerException("Error while updating the user on auth server")
        }
    }

    /**
     * Creates a new user or updates a currently existing one
     */
    @PostMapping
    fun createOrUpdate(request: HttpServletRequest, @RequestBody user: User): Response<User> {
        val callerUser = denyIfNotValidRoles(
            request,
            listOf(
                Rol.GLOBAL_ADMIN,
                Rol.LOCAL_ADMIN,
                Rol.TEACHER,
                Rol.CLINICIAN,
                Rol.CHILD,
                Rol.PARENT
            )
        )

        val isChallenges = userAgentIs(request.getHeader(HttpHeaders.USER_AGENT), "challenges")

        if (callerUser.hasRol(Rol.GLOBAL_ADMIN) && !isChallenges) {
            throw ForbiddenException("The user cant access")
        }

        if (user.email !== null) {
            // we are creating teacher, a doctor or an admin. Check if the user already exists before proceeding
            val preUserOpt = userRepo.getByEmail(user.email!!)
            if (preUserOpt.isPresent) {
                return preUserOpt.get().let { prevUser ->
                    if (user.entityIds?.isNotEmpty() == true) {
                        user.entityIds?.singleOrNull()?.let {
                            var entities = prevUser.entityIds ?: listOf()
                            if (!entities.contains(it)) {
                                entities = entities.plus(it);
                            }
                            prevUser.entityIds = entities
                        }
                    }
                    user.roles?.singleOrNull()?.let {
                        if (prevUser.roles?.contains(it) == false) {
                            prevUser.roles = (prevUser.roles ?: emptyList()).plus(it)

                        }
                    }
                    userRepo.save(prevUser)
                    Response(prevUser, 200)
                }
            }
        }

        val userId = user.id
        val result = if (userId == null) {
            val created = create(callerUser, user)
            createKeycloakUser(created)
            recalculateChallengesFor(request.getHeader(HttpHeaders.AUTHORIZATION), created)
            created
        } else {
            val isTeacherOrClinician =
                callerUser.hasRol(Rol.TEACHER) || callerUser.hasRol(Rol.CLINICIAN)
            if (!isTeacherOrClinician && !isChallenges) {
                throw ForbiddenException("The user can't update")
            }

            val updated = update(callerUser, userId, user, isChallenges)
            //TODO iarjona: Recalculate if required (changes on entities or groups?)
            //recalculateChallengesFor(token, updated)
            updated
        }

        if (result.hasRol(Rol.PARENT)) {
            refreshParentListOfCaringForIds(result)
        }
        return Response(result)

    }

    /**
     * Helper method that adds a reference to a parent user in a list of child users
     */
    private fun refreshParentListOfCaringForIds(parent: User) {
        val parentId = parent.id ?: throw DataException("Parent has no id")
        val childIdList = parent.caringForIds ?: emptyList()
        val childListToUpdate = userRepo.findAllById(childIdList).onEach {
            var parentList = it.parents ?: listOf(parentId)
            if (!parentList.contains(parentId)) {
                parentList = parentList.plus(parentId)
            }
            it.parents = parentList
        }
        userRepo.saveAll(childListToUpdate)
    }

    private fun recalculateChallengesFor(token: String, user: User) {
        if (!user.hasRol(Rol.CHILD)) {
            return
        }

        val response = tools.http.doPOST(
            token,
            "${tools.challengesEndpoint}/challengeProgramming/refresh",
            user
        )

        if (response.statusCodeValue != HttpStatus.OK.value()) {
            userRepo.delete(user)
            val body = response.body as Response<*>
            throw ServerException(body.message)
        }
    }

    private fun createKeycloakUser(user: User) {
        val isParent = user.hasRol(Rol.PARENT)
        val isChild = user.hasRol(Rol.CHILD)

        val kcAttributes = toKeyCloakAttributes(user)

        val kcUser = UserRepresentation()
        kcUser.attributes = kcAttributes
        kcUser.email = user.email
        if (isParent || isChild) {
            kcUser.isEnabled =
                false //No credentials. We will enable and set user on registration process
            kcUser.username = user.id.toString()
        } else {
            //The password wont be change by user if locally mailing disable, e.x local environment with mailing disabled
            val newPassword = FullRandomString.nextString(16).takeIf {
                keycloakMailingEnabled.toBoolean()
            } ?: user.email.toString() //Avoid sending mail on local or test environment
            kcUser.credentials = keycloakHelper.newCredentialPasswordType(newPassword)
            kcUser.isEnabled = true
            kcUser.username = user.email
        }

        val createResponse =
            keycloakHelper.getClient().realm(keycloakPAIDORealm).users().create(kcUser)
        if (createResponse.status != 201) {
            // TODO: if user existed previously, we delete a valid user
            userRepo.delete(user)
            throw AuthServerException("Error while creating the user on auth server (${createResponse.statusInfo.statusCode} ${createResponse.statusInfo.reasonPhrase})")
        }

        if (keycloakMailingEnabled.toBoolean() && !isParent && !isChild) {
            sendUpdatePasswordEmailTo(kcUser.username)
        }
    }

    private fun toKeyCloakAttributes(user: User): Map<String, List<String>> {
        val kcAttributes = mutableMapOf<String, List<String>>()
        kcAttributes["_id"] = listOf(user.id.toString())
        kcAttributes["caring_for"] = listOf(
            tools.jackson.writeValueAsString(
                user.caringForIds
                    ?: emptyArray<ObjectId>()
            )
        )
        return kcAttributes
    }

    private fun updateKeycloakPasswordForUser(username: String, password: String): Boolean {
        return try {
            val kcResult =
                keycloakHelper.getClient().realm(keycloakPAIDORealm).users().search(username)

            when {
                kcResult.isEmpty() -> throw AuthServerException("No keycloak user was found")
                kcResult.size > 1 -> throw AuthServerException("Multiple users found with username: $username.")
                else -> kcResult.first()
            }.apply {
                credentials = keycloakHelper.newCredentialPasswordType(password)
            }.also {
                keycloakHelper.getClient().realm(keycloakPAIDORealm).users().get(it.id).update(it)
            }

            true
        } catch (e: Exception) {
            logger.error(e) {
                "Error while updating user on keycloak. (userId: $username)"
            }

            false
        }
    }

    private fun updateKeycloakAttributesFromId(userId: ObjectId): Boolean {
        return try {
            val user = userRepo.findById(userId).orElseThrow {
                throw DataException("No user was found for: $userId")
            }
            updateKeycloakAttributes(user)
        } catch (e: Exception) {
            logger.error(e) {
                "Error while updating user on keycloak. (userId: ${userId})"
            }
            false
        }

    }

    private fun updateKeycloakAttributes(user: User): Boolean {
        return try {
            val username = user.id.toString()
                .takeIf { user.hasRol(Rol.PARENT) || user.hasRol(Rol.CHILD) }
                ?: user.email.toString()
            val kcResult =
                keycloakHelper.getClient().realm(keycloakPAIDORealm).users().search(username)
            when {
                kcResult.isEmpty() -> throw AuthServerException("No keycloak user was found")
                kcResult.size > 1 -> throw AuthServerException("Multiple users found with username: $username.")
                else -> kcResult.first()
            }.apply {
                attributes = toKeyCloakAttributes(user)
            }.also {
                keycloakHelper.getClient().realm(keycloakPAIDORealm).users().get(it.id).update(it)
            }

            true
        } catch (e: Exception) {
            logger.error(e) {
                "Error while updating user on keycloak. (userId: ${user.id})"
            }
            false
        }
    }

    @PostMapping("/fixAllUsersKeycloak")
    fun fixAllKeycloak(
        request: HttpServletRequest
    ): Response<Int> {
        denyIfNotValidRoles(request, listOf(Rol.GLOBAL_ADMIN))
        val users = userRepo.findWithOptionalFilters()
        val count = users.data.map(::updateKeycloakAttributes).count { it }
        return Response(count)
    }

    private fun sendUpdatePasswordEmailTo(username: String): Boolean {
        val searchResult = keycloakHelper.getClient()
            .realm(keycloakPAIDORealm).users().search(username)

        val kcId = if (searchResult.isEmpty()) {
            throw AuthServerException("No keycloak user was found")
        } else {
            searchResult.first().id
        }

        return try {
            keycloakHelper.getClient()
                .realm(keycloakPAIDORealm).users().get(kcId)
                .executeActionsEmail(listOf("UPDATE_PASSWORD"))
            true
        } catch (e: Exception) {
            logger.error(e) {
                "Error while sending email to keycloak user '$kcId'. Remember to manually activate or send email on Keycloak panel. ${e.message}"
            }
            false
        }
    }

    @GetMapping("/available")
    fun available(@RequestParam displayName: String): Response<Boolean> {
        return Response(!userRepo.existsByDisplayName(displayName))
    }

    /**
    0. Mobile ask for user BY registration code
    0.5. If user already registered, return, else continue
    1. Randomize a new password (temporary)
    2. Find keycloak user by username
    3. Set keycloak user password with randomized
    4. Response user with username + randomized password
    5. Wait few seconds, check keycloak user has logged in. Then, remove registration code. Else, invalidate tmp passwd
     */
    @GetMapping("/credentials")
    fun credentials(
        @RequestParam("registrationCode") code: String
    ): Response<Credentials> {
        val storedUserOptional = userRepo.findUserByRegCodes(userRegistrationCode = code)
        if (storedUserOptional.isPresent) {
            val storedUser = storedUserOptional.get()
            return Response(
                Credentials(
                    username = storedUser.resolveUserId().toString(),
                    isAlreadyRegistered = true
                )
            )
        }

        val user = userRepo.findUserByRegCodes(registrationCode = code).orElseThrow {
            throw NotFoundException("No user was found with '$code' registration code. Is the user registered already or incorrect registration code provided?")
        }

        val userName = user.id.toString()
            .takeIf { user.hasRol(Rol.PARENT) || user.hasRol(Rol.CHILD) } ?: user.email.toString()

        val kcResult = keycloakHelper.getClient().realm(keycloakPAIDORealm).users().search(userName)

        val kcUser = when {
            kcResult.isEmpty() -> throw AuthServerException("No keycloak user was found")
            kcResult.size > 1 -> throw AuthServerException("Multiple users have '$code' registration code.")
            else -> kcResult.first()
        }

        val userRoles = user.roles ?: emptyList()
        val newRandomPsw = FullRandomString.nextString(16)
        val credentials = Credentials(kcUser.username, newRandomPsw, userRoles, false)

        //Update with random password
        kcUser.isEnabled = true
        kcUser.credentials = keycloakHelper.newCredentialPasswordType(newRandomPsw)
        keycloakHelper.getClient().realm(keycloakPAIDORealm).users().get(kcUser.id).update(kcUser)

        thread {
            //Wait to check if mobile has logged in
            Thread.sleep(credentialsSecondsToCheck.toLong() * 1000)

            val sessions = keycloakHelper.getClient().realm(keycloakPAIDORealm).users()
                .get(kcUser.id).userSessions
            if (sessions.isEmpty()) {
                //User has not logged in, so invalidate user
                kcUser.isEnabled = false
                keycloakHelper.getClient().realm(keycloakPAIDORealm).users().get(kcUser.id)
                    .update(kcUser)
            } else {
                //User has logged in, so invalidate registration code
                if (credentialsExpires.toBoolean()) {
                    user.userRegistrationCode = user.registrationCode
                    user.registrationCode = null
                    user.registrationCodeExpiresAt = null
                    userRepo.save(user)
                }
            }
        }

        return Response(credentials)
    }
}