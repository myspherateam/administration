package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.*
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/entities")
class EntitiesController : AbstractController() {

    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) page: Int?,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) pageSize: Int?
    ): Response<List<Entity>> {
        val callerUser = denyIfNotValidRoles(
            request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val result = when {
            callerUser.hasRol(Rol.LOCAL_ADMIN) -> {
                entityRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    creator = callerUser.resolveUserId()
                )
            }
            else -> {
                val entitiesAllowed = callerUser.entityIds ?: emptyList()
                entityRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    entityIds = entitiesAllowed
                )
            }
        }
        return Response(result.data, count = result.total)
    }

    @GetMapping("/{id}")
    fun getOne(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<Entity> {
        val callerUser = denyIfNotValidRoles(
            request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.LOCAL_ADMIN, Rol.PARENT, Rol.CHILD)
        )

        val result = when {
            callerUser.hasRol(Rol.LOCAL_ADMIN) -> {
                val entities = entityRepo
                    .findWithOptionalFilters(
                        entityIds = listOf(id),
                        creator = callerUser.resolveUserId()
                    )
                if (entities.data.isEmpty()) {
                    throw NotFoundException("No entity was found")
                }
                entities.data.first()
            }
            else -> {
                val entitiesAllowed = callerUser.entityIds ?: emptyList()
                if (!entitiesAllowed.contains(id)) {
                    throw ForbiddenException("The user cant read")
                }
                entityRepo.findById(id)
                    .orElseThrow { throw NotFoundException("No entity was found") }
            }
        }
        return Response(result)
    }

    @PostMapping
    fun createOne(request: HttpServletRequest, @RequestBody entity: Entity): Response<Entity> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.LOCAL_ADMIN))

        with(entity) {
            creator = callerUser.resolveUserId()

            type = type?.let {
                val typeFiltered = it.toLowerCase().trim()
                if (!EntityType.types.contains(typeFiltered)) {
                    throw BadRequestException("type is not valid (Example: ${EntityType.typesStr})")
                }
                typeFiltered
            } ?: throw BadRequestException("type is required")

            name?.let {
                val nameFiltered = it.trim()
                if (nameFiltered.isEmpty()) {
                    throw BadRequestException("name is not valid")
                }
                nameFiltered
            } ?: throw BadRequestException("name is required")

            acronym?.let {
                val acronymFiltered = it.trim()
                if (acronymFiltered.isEmpty()) {
                    throw BadRequestException("acronym is not valid")
                }
                acronymFiltered
            } ?: throw BadRequestException("acronym is required")
        }

        val response = entityRepo.save(entity);

        callerUser.entityIds = callerUser.entityIds?.plus(response.id!!)
        userRepo.save(callerUser)

        return Response(entityRepo.save(entity))
    }

    @GetMapping("/{id}/stats")
    fun getEntityStatistics(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<EntityStatistics> {
        val callerUser = denyIfNotValidRoles(
            request,
            listOf(Rol.GLOBAL_ADMIN, Rol.LOCAL_ADMIN, Rol.TEACHER, Rol.CLINICIAN)
        )

        val canRead = callerUser.entityIds?.contains(id) ?: false
        if (!canRead) {
            throw ForbiddenException("The user cant read")
        }

        val users = userRepo.findWithOptionalFilters(entityIds = listOf(id)).data
        val numChildren = users.count { it.roles?.contains(Rol.CHILD.type) == true }
        val numParents = users.count { it.roles?.contains(Rol.PARENT.type) == true }
        val numClinicians = users.count { it.roles?.contains(Rol.CLINICIAN.type) == true }
        val numTeachers = users.count { it.roles?.contains(Rol.TEACHER.type) == true }
        val numSteps = users.sumBy { it.steps ?: 0 }
        val numAssignedChallenges = users.map { it.numAssignedChallenges }
            .reduce { acc, numChallenges -> (acc ?: NumChallenges.empty).plus(numChallenges) }
            ?: NumChallenges.empty
        val numCompletedChallenges = users.map { it.numCompletedChallenges }
            .reduce { acc, numChallenges -> (acc ?: NumChallenges.empty).plus(numChallenges) }
            ?: NumChallenges.empty
        val potentialPoints = users.sumBy { it.potentialPoints ?: 0 }
        val points = users.sumBy { it.points ?: 0 }

        val numGroups = groupRepo.findWithOptionalFilters(entityIds = listOf(id)).total.toInt()

        return Response(
            EntityStatistics(
                id = id,
                numGroups = numGroups,
                numClinicians = numClinicians,
                numTeachers = numTeachers,
                numChildren = numChildren,
                numParents = numParents,
                numSteps = numSteps,
                numAssignedChallenges = numAssignedChallenges,
                numCompletedChallenges = numCompletedChallenges,
                potentialPoints = potentialPoints,
                points = points
            )
        )
    }
}