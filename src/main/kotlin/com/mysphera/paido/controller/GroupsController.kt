package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.Group
import com.mysphera.paido.data.GroupStatistics
import com.mysphera.paido.data.NumChallenges
import com.mysphera.paido.data.Rol
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/groups")
class GroupsController : AbstractController() {

    @GetMapping
    fun getMany(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "page",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) page: Int?,
        @Parameter(
            `in` = ParameterIn.QUERY,
            name = "pageSize",
            required = false,
            schema = Schema(type = "integer")
        )
        @RequestParam(required = false) pageSize: Int?,
        @RequestParam(required = false) entityIds: List<ObjectId>?,
        @RequestParam(required = false) allGroups: Boolean?
    ): Response<List<Group>> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.PARENT, Rol.CHILD))

        val isTeacher = callerUser.hasRol(Rol.TEACHER)
        val isClinician = callerUser.hasRol(Rol.CLINICIAN)

        val result = when {
            (isTeacher || isClinician) && entityIds.isNullOrEmpty() -> {
                throw BadRequestException("entityIds is required")
            }
            (isTeacher || isClinician) && !entityIds.isNullOrEmpty() -> {
                when (allGroups) {
                    false, null -> groupRepo.findWithOptionalFilters(
                        getPageRequest(page, pageSize),
                        creator = callerUser.resolveUserId(),
                        entityIds = entityIds
                    )
                    true -> groupRepo.findWithOptionalFilters(
                        getPageRequest(page, pageSize), entityIds = entityIds
                    )
                }
            }
            else -> {
                val groupsAllowed = callerUser.groupIds ?: emptyList()
                groupRepo.findWithOptionalFilters(
                    getPageRequest(page, pageSize),
                    groupsAllowed,
                    entityIds
                )
            }
        }

        // Group list
        val groupIds = result.data.map { it.id ?: throw DataException("Group has no id") }
        // Find all users for all groups
        userRepo.findWithOptionalFilters(groupIds = groupIds).data.forEach { user ->
            val userPoints = user.points ?: 0

            // For each group of the user
            user.groupIds?.forEach { groupId ->
                // Find group with the same id as each of the user's group
                result.data.find { group -> group.id == groupId }.apply {
                    this?.usersAmount = this?.usersAmount?.plus(1) ?: 1
                    this?.pointsAmount = userPoints.plus(this?.pointsAmount ?: 0)
                }
            }
        }

        return Response(result.data, count = result.total)
    }

    @GetMapping("/{id}")
    fun getOne(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<Group> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.PARENT, Rol.CHILD))

        return Response(when {
            callerUser.hasRol(Rol.TEACHER) || callerUser.hasRol(Rol.CLINICIAN) -> {
                val result = groupRepo.findWithOptionalFilters(
                    groupIds = listOf(id),
                    creator = callerUser.resolveUserId()
                )
                if (result.data.isEmpty()) {
                    throw NotFoundException("No group was found")
                }
                result.data.first()
            }
            else -> {
                val canRead = callerUser.groupIds?.contains(id) ?: false
                if (!canRead) {
                    throw ForbiddenException("The user cant read")
                }
                groupRepo.findById(id).orElseThrow { throw NotFoundException("No group was found") }
            }
        })
    }

    @PostMapping
    fun createOne(request: HttpServletRequest, @RequestBody group: Group): Response<Group> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        with(group) {
            creator = callerUser.resolveUserId()

            entityId?.let {
                if (!entityRepo.existsById(it)) {
                    throw BadRequestException("The entityId provided doesn't exists")
                }
            } ?: throw BadRequestException("entityId is required")

            name?.let {
                val nameFiltered = it.trim()
                if (nameFiltered.isEmpty()) {
                    throw BadRequestException("name is not valid")
                }
            } ?: throw BadRequestException("name is required")

            name = name?.trim() ?: throw BadRequestException("name is required")
        }

        return Response(groupRepo.save(group))
    }

    @PatchMapping("/{id}")
    fun editOne(
        request: HttpServletRequest, @RequestBody group: Group,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<Group> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val result = groupRepo.findWithOptionalFilters(
            groupIds = listOf(id),
            creator = callerUser.resolveUserId()
        )
        if (result.data.isEmpty()) {
            throw NotFoundException("No group was found")
        }
        val oldGroup = result.data.first()

        val nameFiltered = group.name?.trim()
        if (nameFiltered?.isEmpty() == true) {
            throw BadRequestException("name is not valid")
        }
        oldGroup.name = nameFiltered ?: oldGroup.name


        return Response(groupRepo.save(oldGroup))
    }

    @DeleteMapping("/{id}")
    fun deleteOne(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<Unit> {
        val callerUser = denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN))

        val result = groupRepo.findWithOptionalFilters(
            groupIds = listOf(id),
            creator = callerUser.resolveUserId()
        )
        if (result.data.isEmpty()) {
            throw NotFoundException("No group was found")
        }

        val group = result.data.first()

        val groupId = group.id ?: throw DataException("Group has no id")

        val numUsers = userRepo.findWithOptionalFilters(groupIds = listOf(groupId)).total

        if (numUsers > 0) {
            throw BadRequestException("The group must be empty")
        }

        groupRepo.delete(group)

        return Response(Unit)
    }

    @GetMapping("/{id}/stats")
    fun getGroupStatistics(
        request: HttpServletRequest,
        @Parameter(
            `in` = ParameterIn.PATH,
            name = "id",
            allowEmptyValue = false,
            schema = Schema(type = "string")
        )
        @PathVariable("id") id: ObjectId
    ): Response<GroupStatistics> {
        val callerUser =
            denyIfNotValidRoles(request, listOf(Rol.TEACHER, Rol.CLINICIAN, Rol.PARENT, Rol.CHILD))

        when {
            callerUser.hasRol(Rol.TEACHER) || callerUser.hasRol(Rol.CLINICIAN) -> {
                val result = groupRepo.findWithOptionalFilters(
                    groupIds = listOf(id),
                    creator = callerUser.resolveUserId()
                )
                if (result.data.isEmpty()) {
                    throw NotFoundException("No group was found")
                }
                result.data.first()
            }
            else -> {
                val canRead = callerUser.groupIds?.contains(id) ?: false
                if (!canRead) {
                    throw ForbiddenException("The user cant read")
                }
                groupRepo.findById(id).orElseThrow { throw NotFoundException("No group was found") }
            }
        }

        val users = userRepo.findWithOptionalFilters(groupIds = listOf(id)).data
        val numChildren = users.count { it.roles?.contains(Rol.CHILD.type) == true }
        val numParents = users.count { it.roles?.contains(Rol.PARENT.type) == true }
        val numSteps = users.sumBy { it.steps ?: 0 }
        val numAssignedChallenges = users.map { it.numAssignedChallenges }
            .fold(NumChallenges.empty) { acc, numChallenges -> acc.plus(numChallenges) }
        val numCompletedChallenges = users.map { it.numCompletedChallenges }
            .fold(NumChallenges.empty) { acc, numChallenges -> acc.plus(numChallenges) }
        val potentialPoints = users.sumBy { it.potentialPoints ?: 0 }
        val points = users.sumBy { it.points ?: 0 }

        return Response(
            GroupStatistics(
                id = id,
                numChildren = numChildren,
                numParents = numParents,
                numSteps = numSteps,
                numAssignedChallenges = numAssignedChallenges,
                numCompletedChallenges = numCompletedChallenges,
                potentialPoints = potentialPoints,
                points = points
            )
        )
    }
}