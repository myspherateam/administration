package com.mysphera.paido.controller

import com.mysphera.paido.DataException
import com.mysphera.paido.ForbiddenException
import com.mysphera.paido.MicroServicesToolbox
import com.mysphera.paido.data.*
import com.mysphera.paido.getJWTPayload
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
abstract class AbstractController {
    protected val logger = KotlinLogging.logger { }

    @Autowired
    protected lateinit var tools: MicroServicesToolbox

    @Autowired
    protected lateinit var countersRepo: CountersRepository

    @Autowired
    protected lateinit var groupRepo: GroupRepository

    @Autowired
    protected lateinit var entityRepo: EntityRepository

    @Autowired
    protected lateinit var userRepo: UserRepository

    fun denyIfNotValidRoles(request: HttpServletRequest, validRoles: List<Rol>): User {
        val callerId = request.getJWTPayload().id

        return userRepo.findById(callerId)
            .orElseThrow { throw DataException("Data error with user: $callerId") }
            .apply {
                validRoles.find { hasRol(it) }
                    ?: throw ForbiddenException("Allowed user roles: ${validRoles.joinToString(", ")}")
            }
    }

    fun userAgentIs(userAgent: String?, value: String): Boolean {
        return userAgent?.toLowerCase() == value
    }
}