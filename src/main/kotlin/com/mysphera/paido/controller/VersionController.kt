package com.mysphera.paido.controller

import com.mysphera.paido.*
import com.mysphera.paido.data.User
import com.mysphera.paido.data.VersionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestController
@RequestMapping("/version")
class VersionController : AbstractController() {
    companion object {
        private const val ANDROID_PLATFORM = "android"
        private const val IOS_PLATFORM = "ios"
        private val versionPattern = Regex("^[0-9]+\\.[0-9]+\\.[0-9]+$")
    }

    @Autowired
    private lateinit var versionRepository: VersionRepository

    /**
     * Check version and returns:
     *      1   if user's current version is GREATER OR EQUAL THAN current version (User can access)
     *      -1  if user's current version is LOWER THAN accepted version (User shouldn't access)
     *      0   if user's current version is BETWEEN accepted and current version (User must update)
     */
    @GetMapping("/check")
    fun checkVersion(
        request: HttpServletRequest,
        @RequestParam platform: String,
        @RequestParam currentVersion: String
    ): Response<Int> {
        val latestVersion = versionRepository.getLatestVersion()

        val storedAcceptedVersion: String
        val storedCurrentVersion: String
        when (platform.toLowerCase()) {
            ANDROID_PLATFORM -> {
                storedAcceptedVersion = latestVersion.version.androidAcceptedVersion
                    ?: throw DataException("androidAcceptedVersion")
                storedCurrentVersion = latestVersion.version.androidCurrentVersion
                    ?: throw DataException("androidCurrentVersion")
            }
            IOS_PLATFORM -> {
                storedAcceptedVersion = latestVersion.version.iosAcceptedVersion
                    ?: throw DataException("iosAcceptedVersion")
                storedCurrentVersion = latestVersion.version.iosCurrentVersion
                    ?: throw DataException("iosCurrentVersion")
            }
            else -> {
                throw BadRequestException("platform '$platform' is not valid")
            }
        }

        if (!currentVersion.matches(versionPattern)) {
            throw BadRequestException("currentVersion is not following 'major.minor.patch' pattern. Only digits allowed.")
        }
        val storedAcceptedVersionSplit = storedAcceptedVersion.split(".")
        val storedCurrentVersionSplit = storedCurrentVersion.split(".")
        if (storedAcceptedVersionSplit.size != 3 || storedCurrentVersionSplit.size != 3) {
            throw DataException("Stored versions doesn't follow 'major.minor.patch' pattern.")
        }

        val userCurrentVersionFiltered = currentVersion.split(".")
            .joinToString(""){ it.padStart(3, '0') }
        val storedAcceptedVersionFiltered = storedAcceptedVersionSplit
            .joinToString(""){ it.padStart(3, '0') }
        val storedCurrentVersionFiltered = storedCurrentVersionSplit
            .joinToString(""){ it.padStart(3, '0') }
        return when {
            userCurrentVersionFiltered >= storedCurrentVersionFiltered -> {
                Response(1)
            }
            userCurrentVersionFiltered < storedAcceptedVersionFiltered -> {
                Response(-1)
            }
            userCurrentVersionFiltered in storedAcceptedVersionFiltered..storedCurrentVersionFiltered -> {
                Response(0)
            }
            else -> {
                Response(-1)
            }
        }
    }

    @PostMapping("/register")
    fun registerVersion(
        request: HttpServletRequest,
        @RequestBody userVersion: UserVersion
    ): Response<User> {
        val caller = request.getJWTPayload().id
        val callerUser = userRepo.findById(caller).orElseThrow {
            throw DataException("Data error with user: $caller")
        }.apply {
            mobilePlatform = userVersion.platform?.toLowerCase()
            mobileVersion = userVersion.currentVersion

            if (mobilePlatform != ANDROID_PLATFORM && mobilePlatform != IOS_PLATFORM) {
                throw BadRequestException("platform '$mobilePlatform' is not valid")
            }
        }

        return Response(userRepo.save(callerUser))
    }
}