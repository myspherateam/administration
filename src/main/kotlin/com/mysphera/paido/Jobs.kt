package com.mysphera.paido

import com.mysphera.paido.data.UserRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Suppress("unused")
@Component
class UserScheduler {
    private val logger = KotlinLogging.logger {}

    @Autowired
    private lateinit var userRepository: UserRepository

    @Scheduled(cron = "\${user-scheduling.reg-code-clean-up.cron}")
    fun regCodeCleanUp() {
        logger.info { "Starting 'user-scheduling.reg-code-clean-up' job" }
        val storedUsers = userRepository.findUserWithExpiredRegistrationCode().onEach {
            it.registrationCode = null
            it.registrationCodeExpiresAt = null
        }
        val updatedUsers = userRepository.saveAll(storedUsers)
        logger.info { "Expired ${updatedUsers.size} user's reg codes" }
    }
}
