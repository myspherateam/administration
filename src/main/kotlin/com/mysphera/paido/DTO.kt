package com.mysphera.paido

import org.bson.types.ObjectId

data class JWTPayload(
    val id: ObjectId,
    val originalId: ObjectId,
    val caringFor: List<ObjectId>
)

data class Credentials(
    val username: String? = null,
    val password: String? = null,
    val roles: List<String>? = null,
    val isAlreadyRegistered: Boolean? = null
)

data class Response<T>(
    var message: T,
    var status: Int = 200,
    val statusMessage: String? = "OK",
    var count: Long? = null,
    var errorCode: String? = null
)

data class PaginatedData<T>(
    var data: T,
    var total: Long
)

data class UserVersion(
    var platform: String? = null,
    var currentVersion: String? = null
)
