package com.mysphera.paido

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@SpringBootApplication
@EnableScheduling
@EnableSwagger2
class PAIDOApplication : WebMvcConfigurer {
    @Autowired
    private lateinit var authInterceptor: PAIDOInterceptor

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(authInterceptor).addPathPatterns(mutableListOf("/groups/**"))
        registry.addInterceptor(authInterceptor).addPathPatterns(mutableListOf("/entities/**"))
        registry.addInterceptor(authInterceptor).addPathPatterns(mutableListOf("/users/**"))
            .excludePathPatterns("/users/lostPassword")
            .excludePathPatterns("/users/credentials")
            .excludePathPatterns("/users/available")
        registry.addInterceptor(authInterceptor).addPathPatterns(mutableListOf("/version/**"))
            .excludePathPatterns("/version/check")
        registry.addInterceptor(authInterceptor).addPathPatterns(mutableListOf("/me/**"))
    }
}

fun main(args: Array<String>) {
    //SpringApplication.run(PAIDOApplication::class.java, *args)
    runApplication<PAIDOApplication>(*args)
}

@Bean
fun restApiSwagger() :Docket{
    return Docket(DocumentationType.SWAGGER_2).select().apis(
        RequestHandlerSelectors.basePackage("com.mysphera.paido")
    ).build()
}
