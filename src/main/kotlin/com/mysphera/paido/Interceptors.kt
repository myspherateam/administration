package com.mysphera.paido

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class PAIDOInterceptor : HandlerInterceptor {
    @Autowired
    private lateinit var authenticator: PAIDOAuthenticator

    @Autowired
    lateinit var lastLoginCache: LastLoginCache

    @Throws(Exception::class)
    override fun preHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any
    ): Boolean {
        val payload = authenticator.requestToPayload(request)

        lastLoginCache.setJustSeen(payload.id, payload.originalId != payload.id)
        lastLoginCache.setJustSeen(payload.originalId, false)

        with(request.session) {
            setAttribute("jwtPayload", payload)
        }

        return true
    }
}

fun HttpServletRequest.getJWTPayload(): JWTPayload {
    return this.session.getAttribute("jwtPayload") as? JWTPayload
        ?: throw MissingAuthenticationException("Incomplete payload data")
}