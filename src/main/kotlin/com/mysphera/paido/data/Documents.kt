package com.mysphera.paido.data

import com.mysphera.paido.DataException
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Schema
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Suppress("unused")
enum class EntityType(val type: String){
    CLINIC("clinic"),
    SCHOOL("school"),
    ;

    companion object {
        val types = values().map { it.type.toLowerCase().trim() }
        val typesStr = types.joinToString(", ")
    }

    override fun toString(): String {
        return this.type
    }
}

enum class Rol(
    val type: String,
    val manages: List<Rol> = emptyList()
){
    CHALLENGE_EDITOR("ChallengeEditor"),
    CHILD("Child"),
    PARENT("Parent"),
    TEACHER("Teacher", listOf(CHILD, PARENT)),
    CLINICIAN("Clinician", listOf(CHILD, PARENT)),
    LOCAL_ADMIN("LocalAdmin", listOf(TEACHER, CLINICIAN)),
    GLOBAL_ADMIN("GlobalAdmin", listOf(LOCAL_ADMIN)),
    ;


    override fun toString(): String {
        return this.type
    }

    companion object {
        fun fromString(value: String): Rol? {
            return when (value) {
                CHALLENGE_EDITOR.type -> CHALLENGE_EDITOR
                CHILD.type -> CHILD
                PARENT.type -> PARENT
                TEACHER.type -> TEACHER
                CLINICIAN.type -> CLINICIAN
                LOCAL_ADMIN.type -> LOCAL_ADMIN
                GLOBAL_ADMIN.type -> GLOBAL_ADMIN
                else -> return null
            }
        }

        fun manages(roleList: List<String>?): List<Rol> =
            toListOfRoles(roleList)
                .flatMap { it.manages }
                .distinct()

        fun toListOfRoles(roleList: List<String>?): List<Rol> =
            roleList
                ?.mapNotNull {
                    try {
                        fromString(it)
                    } catch (e: IllegalArgumentException) {
                        null
                    }
                }
                ?.distinct()
                ?: listOf()
    }
}

@Document("Group")
open class Group(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    @field:Schema(type = "string")
    var entityId: ObjectId? = null,
    var name: String? = null,
    @field:Schema(type = "string")
    var creator: ObjectId? = null
) {
    @Transient
    var usersAmount: Int? = null

    @Transient
    var pointsAmount: Int? = null
}

data class GroupStatistics(
    var id: ObjectId,
    var numChildren: Int,
    var numParents: Int,
    var numSteps: Int,
    var numAssignedChallenges: NumChallenges,
    var numCompletedChallenges: NumChallenges,
    var potentialPoints: Int,
    var points: Int
)

data class GCSPoint (
    var lon: Float,
    var lat: Float
)

@Suppress("unused")
@Document("Entity")
open class Entity(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    var type: String? = null,
    var name: String? = null,
    var acronym: String? = null,
    @field:Schema(type = "string")
    var creator: ObjectId? = null,
    var country: String? = null,
    var postalCode: String? = null,
    var location: GCSPoint? = null
)

data class EntityStatistics(
    var id: ObjectId,
    var numGroups: Int,
    var numClinicians: Int,
    var numTeachers: Int,
    var numChildren: Int,
    var numParents: Int,
    var numSteps: Int,
    var numAssignedChallenges: NumChallenges,
    var numCompletedChallenges: NumChallenges,
    var potentialPoints: Int,
    var points: Int
)

data class NumChallenges(
    val physical: Int,
    val authority: Int,
    val gps: Int,
    val questionnaire: Int,
    val news: Int
) {
    fun plus(other: NumChallenges?): NumChallenges {
        if (other == null) {
            return this
        }
        return NumChallenges(
            physical + other.physical,
            authority + other.authority,
            gps + other.gps,
            questionnaire + other.questionnaire,
            news + other.news
        )
    }
    companion object {
        val empty = NumChallenges(0,0,0,0,0)
    }
}

@Suppress("unused")
@Document("User")
open class User(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    var roles: List<String>? = null,
    var potentialPoints: Int? = null,
    var points: Int? = null,
    var steps: Int? = null,
    var numAssignedChallenges: NumChallenges? = null,
    var numCompletedChallenges: NumChallenges? = null,
    var email: String? = null,
    var userRegistrationCode: String? = null,
    var registrationCode: String? = null,
    var registrationCodeExpiresAt: Date? = null,
    var externalId: String? = null,
    var displayId: Int? = null,
    var displayName: String? = null,
    var mobilePlatform: String? = null,
    var mobileVersion: String? = null,
    var badges: List<ObjectId>? = null,
    var futureBadges: List<ObjectId>? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var entityIds: List<ObjectId>? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var groupIds: List<ObjectId>? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var caringForIds: List<ObjectId>? = null,
    @field:ArraySchema(schema = Schema(type = "string"))
    var parents: List<ObjectId>? = null,
    @field:Schema(type = "string")
    var creator: ObjectId? = null,
    var lastSeen: Date? = null,
    var registerState: String? = null
) {
    @Transient
    var parentsUsers: List<User>? = null
    @Transient
    var childsUsers: List<User>? = null
    @Transient
    var password: String? = null

    @Transient
    var ranking: Ranking? = null

    fun resolveUserId(): ObjectId {
        return this.id ?: throw DataException("User has no id")
    }

    fun hasRol(rol: Rol): Boolean {
        return this.roles?.contains(rol.toString()) ?: false
    }

    fun isParentOf(id: ObjectId): Boolean {
        return if (this.hasRol(Rol.PARENT)) {
            this.caringForIds?.contains(id) ?: false
        } else {
            false
        }
    }
}

@Document("Counters")
open class Counters(
    @field:Schema(type = "string")
    @Id var id: ObjectId? = null,
    var userDisplayId: Int? = null
)

@Suppress("unused")
@Document("Versions")
class Versions(
    @Id var id: ObjectId? = null,
    var date: Date? = null,
    var version: Version
)

data class Version(
    var iosAcceptedVersion: String? = null,
    var iosCurrentVersion: String? = null,
    var androidAcceptedVersion: String? = null,
    var androidCurrentVersion: String? =null
)