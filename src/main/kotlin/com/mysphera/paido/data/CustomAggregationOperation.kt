package com.mysphera.paido.data

import org.bson.Document
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext

class CustomAggregationOperation (private val operation: Document): AggregationOperation {

    @Deprecated("Deprecated by the org.springframework.data.mongodb.core.aggregation.AggregationOperation")
    override fun toDocument(aggregationOperationContext: AggregationOperationContext): Document {
        return operation
    }
}