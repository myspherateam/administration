package com.mysphera.paido.data

import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface EntityRepository : MongoRepository<Entity, ObjectId>, EntityDAO
interface UserRepository : MongoRepository<User, ObjectId>, UserDAO
interface GroupRepository : MongoRepository<Group, ObjectId>, GroupDAO
interface CountersRepository : MongoRepository<Counters, ObjectId>, CountersDAO
interface VersionRepository : MongoRepository<Versions, ObjectId>, VersionDAO
