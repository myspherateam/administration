package com.mysphera.paido.data

import com.mysphera.paido.*
import org.bson.Document
import org.bson.types.ObjectId
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.FindAndModifyOptions
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.query.*
import org.springframework.stereotype.Component
import java.util.*

data class UserRanking(val name: String, val value: Double)
data class Ranking(
    val myPosition: Int,
    val myValue: Double,
    val totalUsers: Int,
    val topUsers: List<UserRanking>
)

@Suppress("unused")
enum class RankingType {
    global,
    entity,
    group
}

interface UserDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        ids: List<ObjectId>? = null,
        groupIds: List<ObjectId>? = null,
        entityIds: List<ObjectId>? = null,
        creator: ObjectId? = null,
        roles: List<String>? = null
    ): PaginatedData<List<User>>

    fun existsByDisplayName(displayName: String): Boolean
    fun getFullDisplayName(displayName: String, displayId: Int): String
    fun findUserByRegCodes(
        registrationCode: String? = null,
        userRegistrationCode: String? = null
    ): Optional<User>

    fun findUserWithExpiredRegistrationCode(): List<User>
    fun saveRegisterState(id: ObjectId, lastLoginTime: Long, registerState: String)
    fun addChild(id: ObjectId, childId: ObjectId)
    fun removeChild(id: ObjectId, childId: ObjectId)
    fun addParent(id: ObjectId, parentId: ObjectId)
    fun removeParent(id: ObjectId, parentId: ObjectId)
    fun getRanking(
        id: ObjectId,
        field: String,
        entityIds: List<ObjectId>?,
        groupIds: List<ObjectId>?
    ): Ranking

    fun getByEmail(email: String): Optional<User>
}

@Suppress("unused")
@Component
class UserDAOImpl(private val database: MongoTemplate) : UserDAO {
    override fun findWithOptionalFilters(
        pageable: Pageable?,
        ids: List<ObjectId>?,
        groupIds: List<ObjectId>?,
        entityIds: List<ObjectId>?,
        creator: ObjectId?,
        roles: List<String>?
    ): PaginatedData<List<User>> {
        val query = Query()

        if (ids != null) {
            query.addCriteria(Criteria.where("_id").inValues(ids))
        }
        if (groupIds != null) {
            if (groupIds.isEmpty()) {
                query.addCriteria(Criteria.where("groupIds").size(0))
            } else {
                query.addCriteria(Criteria.where("groupIds").inValues(groupIds))
            }

        }
        if (entityIds != null) {
            query.addCriteria(Criteria.where("entityIds").inValues(entityIds))
        }
        if (creator != null) {
            query.addCriteria(Criteria.where("creator").isEqualTo(creator))
        }
        if (roles != null) {
            query.addCriteria(Criteria.where("roles").inValues(roles))
        }

        val total = database.count(query, User::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, User::class.java)

        return PaginatedData(data, total)
    }

    override fun existsByDisplayName(displayName: String): Boolean {
        val query = Query().addCriteria(Criteria.where("displayName").isEqualTo(displayName))
        val count = database.count(query, User::class.java)
        return count > 0
    }

    override fun getFullDisplayName(displayName: String, displayId: Int): String {
        //Check that the displayName is valid
        val reducedDisplayName = displayName.replace(Regex("[0-9]*"), "")
        if (!Regex("^[A-Z][a-z]*[A-Z][a-z]*$").matches(reducedDisplayName))
            throw BadRequestException("displayName not valid: $reducedDisplayName")

        //Make sure the new displayName is not taken
        if (existsByDisplayName("$reducedDisplayName$displayId")) {
            throw DocumentAlreadyExistsException("User with full displayName ${reducedDisplayName}${displayId} already exists.")
        }

        return "$reducedDisplayName$displayId"
    }

    override fun findUserByRegCodes(
        registrationCode: String?,
        userRegistrationCode: String?
    ): Optional<User> {
        val query = Query()

        if (registrationCode != null) {
            query.addCriteria(Criteria.where("registrationCode").isEqualTo(registrationCode))
        }
        if (userRegistrationCode != null) {
            query.addCriteria(
                Criteria.where("userRegistrationCode").isEqualTo(userRegistrationCode)
            )
        }

        val result = database.findOne(query, User::class.java)
        return Optional.ofNullable(result)
    }

    override fun findUserWithExpiredRegistrationCode(): List<User> {
        val query = Query().addCriteria(Criteria.where("registrationCodeExpiresAt").lt(Date()))
        return database.find(query, User::class.java)
    }

    override fun saveRegisterState(id: ObjectId, lastLoginTime: Long, registerState: String) {
        val query = Query().addCriteria(Criteria.where("_id").isEqualTo(id))
        val update =
            Update().set("lastSeen", Date(lastLoginTime)).set("registerState", registerState)
        database.updateFirst(query, update, User::class.java)
    }

    override fun addChild(id: ObjectId, childId: ObjectId) {
        val query = Query().addCriteria(Criteria.where("_id").isEqualTo(id))
        val update = Update().addToSet("caringForIds", childId)
        database.updateFirst(query, update, User::class.java)
    }

    override fun removeChild(id: ObjectId, childId: ObjectId) {
        val query = Query().addCriteria(Criteria.where("_id").isEqualTo(id))
        val update = Update().pull("caringForIds", childId)
        database.updateFirst(query, update, User::class.java)
    }

    override fun addParent(id: ObjectId, parentId: ObjectId) {
        val query = Query().addCriteria(Criteria.where("_id").isEqualTo(id))
        val update = Update().addToSet("parents", parentId)
        database.updateFirst(query, update, User::class.java)
    }

    override fun removeParent(id: ObjectId, parentId: ObjectId) {
        val query = Query().addCriteria(Criteria.where("_id").isEqualTo(id))
        val update = Update().pull("parents", parentId)
        database.updateFirst(query, update, User::class.java)
    }

    override fun getRanking(
        id: ObjectId,
        field: String,
        entityIds: List<ObjectId>?,
        groupIds: List<ObjectId>?
    ): Ranking {

        database.findById(id, User::class.java) ?: throw NotFoundException("id $id not found")

        val aggregationSteps = mutableListOf<AggregationOperation>()
        // Only get children
        aggregationSteps.add(Aggregation.match(Criteria.where("roles").isEqualTo(Rol.CHILD.type)))
        // when entity request, limit to entity users
        if (entityIds != null) {
            if (entityIds.isEmpty()) {
                throw BadRequestException("User $id has no entities")
            }
            aggregationSteps.add(Aggregation.match(Criteria.where("entityIds").`in`(entityIds)))
        }
        // when group request, limit to group users
        if (groupIds != null) {
            if (groupIds.isEmpty()) {
                throw BadRequestException("User $id has no groups")
            }
            aggregationSteps.add(Aggregation.match(Criteria.where("groupIds").`in`(groupIds)))
        }
        // Add a points percentage field
        aggregationSteps.add(
            CustomAggregationOperation(
                Document(
                    "\$addFields", Document(
                        "pointsPercentage",
                        Document(
                            "\$cond",
                            Document("if", Document("\$gt", listOf("\$potentialPoints", 0)))
                                .append(
                                    "then",
                                    Document("\$divide", listOf("\$points", "\$potentialPoints"))
                                )
                                .append("else", 0)
                        )
                    )
                )
            )
        )
        //aggregationSteps.add(AddFieldsOperation.addField("pointsPercentage").withValueOfExpression("\$divide", "\$points","\$potentialPoints").build())

        // sort descending by either points or pointPercentage
        aggregationSteps.add(CustomAggregationOperation(Document("\$sort", Document(field, -1))))
        //aggregationSteps.add(Aggregation.sort(Sort.by(Sort.Direction.DESC, field)))

        // added the total/percentage value
        aggregationSteps.add(
            Aggregation.facet(
                //CustomAggregationOperation(Document("\$addFields", Document("value", Document("\$toDouble", "\$$field")))),
                CustomAggregationOperation(
                    Document(
                        "\$addFields",
                        Document(
                            "value",
                            Document(
                                "\$convert",
                                Document("input", "\$${field}").append("to", "double")
                                    .append("onError", 0)
                            )
                        )
                    )
                ),
                Aggregation.limit(10)
                //AddFieldsOperation.addField("value").withValueOfExpression("\$toDouble", "\$document.$field").build()
            ).`as`("top").and(
                CustomAggregationOperation(
                    Document(
                        "\$group",
                        Document("_id", "").append("document", Document("\$push", "$\$ROOT"))
                    )
                ),
                //Aggregation.group("0").push("$\$ROOT").`as`("document"),
                CustomAggregationOperation(
                    Document(
                        "\$unwind",
                        Document("path", "\$document").append("includeArrayIndex", "rank")
                    )
                ),
                //Aggregation.unwind("document", "rank"),
                CustomAggregationOperation(Document("\$match", Document("document._id", id))),
                //Aggregation.match(Criteria.where("document._id").isEqualTo(id)),
                CustomAggregationOperation(
                    Document(
                        "\$addFields",
                        Document("value", Document("\$toDouble", "\$document.$field"))
                    )
                )
                //AddFieldsOperation.addField("value").withValueOfExpression("\$toDouble", "\$document.$field").build()
            ).`as`("myValue").and(
                Aggregation.count().`as`("count")
            ).`as`("count")
        )

        data class MyValueType(val document: User, val rank: Int, val value: Double)
        data class UserWithValue(val displayName: String?, val value: Double)
        data class Count(val count: Int)
        data class AggregationResult(
            val top: List<UserWithValue>,
            val myValue: List<MyValueType>,
            val count: List<Count>
        )

        println(
            aggregationSteps.joinToString(
                ",\n",
                prefix = "[\n",
                postfix = "\n]"
            ) { it.toDocument(Aggregation.DEFAULT_CONTEXT).toJson() })

        val result = database.aggregate(
            Aggregation.newAggregation(aggregationSteps),
            User::class.java,
            AggregationResult::class.java
        ).first()

        return Ranking(
            result.myValue.first().rank + 1,
            result.myValue.first().value,
            result.count.first().count,
            result.top.map { UserRanking(it.displayName ?: "", it.value) })
    }

    override fun getByEmail(email: String): Optional<User> {
        val query = Query()
        query.addCriteria(Criteria.where("email").isEqualTo(email))

        val result = database.findOne(query, User::class.java)
        return Optional.ofNullable(result)
    }
}

interface EntityDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        entityIds: List<ObjectId>? = null,
        creator: ObjectId? = null
    ): PaginatedData<List<Entity>>
}

@Suppress("unused")
@Component
class EntityDAOImpl(private val database: MongoTemplate) : EntityDAO {
    override fun findWithOptionalFilters(
        pageable: Pageable?,
        entityIds: List<ObjectId>?,
        creator: ObjectId?
    ): PaginatedData<List<Entity>> {
        val query = Query()

        if (entityIds != null) {
            query.addCriteria(Criteria.where("_id").inValues(entityIds))
        }
        if (creator != null) {
            query.addCriteria(Criteria.where("creator").isEqualTo(creator))
        }

        val total = database.count(query, Entity::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, Entity::class.java)

        return PaginatedData(data, total)
    }
}

interface GroupDAO {
    fun findWithOptionalFilters(
        pageable: Pageable? = null,
        groupIds: List<ObjectId>? = null,
        entityIds: List<ObjectId>? = null,
        creator: ObjectId? = null
    ): PaginatedData<List<Group>>
}

@Suppress("unused")
@Component
class GroupDAOImpl(private val database: MongoTemplate) : GroupDAO {
    override fun findWithOptionalFilters(
        pageable: Pageable?,
        groupIds: List<ObjectId>?,
        entityIds: List<ObjectId>?,
        creator: ObjectId?
    ): PaginatedData<List<Group>> {
        val query = Query()

        if (groupIds != null) {
            query.addCriteria(Criteria.where("_id").inValues(groupIds))
        }
        if (entityIds != null) {
            query.addCriteria(Criteria.where("entityId").inValues(entityIds))
        }
        if (creator != null) {
            query.addCriteria(Criteria.where("creator").isEqualTo(creator))
        }

        val total = database.count(query, Group::class.java)
        if (pageable != null) {
            query.skip(pageable.pageNumber.toLong() * pageable.pageSize.toLong())
            query.limit(pageable.pageSize)
        }

        val data = database.find(query, Group::class.java)

        return PaginatedData(data, total)
    }
}

interface CountersDAO {
    fun incrementAndGetUserDisplayId(): Int
}

@Suppress("unused")
@Component
class CountersDAOImpl(private val database: MongoTemplate) : CountersDAO {
    override fun incrementAndGetUserDisplayId(): Int {
        val update = Update()
        update.inc("userDisplayId", 1)

        val options = FindAndModifyOptions()
        options.returnNew(true)

        val result = database.findAndModify(Query(), update, options, Counters::class.java)

        return result?.userDisplayId ?: initializeUserDisplayId()
    }

    private fun initializeUserDisplayId(): Int {
        val initialCounter = Counters(userDisplayId = 1000)
        val result = database.insert(initialCounter)
        return result.userDisplayId ?: -1
    }
}

interface VersionDAO {
    fun getLatestVersion(): Versions
}

@Suppress("unused")
@Component
class VersionDAOImpl(private val database: MongoTemplate) : VersionDAO {
    override fun getLatestVersion(): Versions {
        val query = Query().with(Sort.by(Sort.Direction.DESC, "_id")).limit(1)
        return database.findOne(query, Versions::class.java)
            ?: throw DataException("No versions found")
    }
}