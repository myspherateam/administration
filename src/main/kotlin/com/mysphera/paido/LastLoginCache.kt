package com.mysphera.paido

import com.mysphera.paido.data.UserRepository
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

data class UserState(var time: Long, var registerState: String)

@Component
class LastLoginCache {

    @Autowired
    private lateinit var userRepository: UserRepository

    private val timeToSendLastSeenSeconds: Long = 30 * 60

    private val users: MutableMap<String, UserState> = mutableMapOf()

    fun setJustSeen(userId: ObjectId, fromOtherAccount: Boolean) {
        val userState: UserState? = users[userId.toString()]
        val currTime = System.currentTimeMillis()
        val newRegisterState = getRegisterState(fromOtherAccount)
        if (userState == null) {
            users[userId.toString()] = UserState(time = currTime, registerState = newRegisterState)
            userRepository.saveRegisterState(userId, currTime, newRegisterState)
        } else {
            var changed = false

            if (userState.time - timeToSendLastSeenSeconds * 1000 < currTime) {
                changed = true
            }
            userState.time = currTime

            if (userState.registerState != newRegisterState) {
                changed = true
            }
            userState.registerState = newRegisterState

            if (changed) {
                userRepository.saveRegisterState(userId, currTime, newRegisterState)
            }
        }
    }
}

fun getRegisterState(fromOtherAccount: Boolean): String =
    if (fromOtherAccount) "registeredFromParent" else "registeredDirectly"
