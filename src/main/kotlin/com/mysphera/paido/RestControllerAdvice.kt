package com.mysphera.paido

import mu.KotlinLogging
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.servlet.http.HttpServletRequest

@Suppress("unused")
@RestControllerAdvice
class RestControllerAdvice : ResponseEntityExceptionHandler() {
    private val log = KotlinLogging.logger {}

    /**
     * Overriding that allows encapsulate that kind of exceptions into our Response object
     */
    override fun handleHttpMessageNotReadable(
        ex: HttpMessageNotReadableException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        val response = Response<Any?>(ex.message, status.value(), status.reasonPhrase)
        return ResponseEntity(response, headers, status)
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenException::class)
    fun handleForbiddenException(
        request: HttpServletRequest,
        ex: ForbiddenException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.FORBIDDEN.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.FORBIDDEN.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(MissingAuthenticationException::class)
    fun handleMissingAuthenticationException(
        request: HttpServletRequest,
        ex: MissingAuthenticationException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.UNAUTHORIZED.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.UNAUTHORIZED.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException::class)
    fun handleBadRequestException(
        request: HttpServletRequest,
        ex: BadRequestException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.BAD_REQUEST.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.BAD_REQUEST.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException::class)
    fun handleElementNotFoundException(
        request: HttpServletRequest,
        ex: NotFoundException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.NOT_FOUND.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.NOT_FOUND.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(AuthServerException::class)
    fun handleAuthServerException(
        request: HttpServletRequest,
        ex: AuthServerException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.INTERNAL_SERVER_ERROR.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServerException::class)
    fun handleServerException(
        request: HttpServletRequest,
        ex: ServerException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.INTERNAL_SERVER_ERROR.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataException::class)
    fun handleDataException(
        request: HttpServletRequest,
        ex: DataException
    ): Response<Any> {
        val errorCode = logException(request, ex)
        return Response(
            ex.value,
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            errorCode = errorCode,
            statusMessage = HttpStatus.INTERNAL_SERVER_ERROR.reasonPhrase
        )
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(DocumentAlreadyExistsException::class)
    fun handleAlreadyExistsException(
        req: HttpServletRequest,
        ex: DocumentAlreadyExistsException
    ): Response<Any> {
        val errorCode = logException(req, ex)
        return Response(
            ex.value, org.apache.http.HttpStatus.SC_CONFLICT,
            statusMessage = HttpStatus.CONFLICT.reasonPhrase,
            errorCode = errorCode
        )
    }

    private fun logException(
        request: HttpServletRequest,
        ex: ValueException
    ): String {
        val errorCode = FullRandomString.nextString(32)
        log.warn(ex) {
            "${ex::class.java.simpleName} catched when handling request to \n${request.method} ${request.requestURI}\nReturned message is: ${ex.value}\nErrorId: $errorCode"
        }

        return errorCode
    }
}