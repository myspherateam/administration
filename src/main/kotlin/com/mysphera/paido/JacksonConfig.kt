package com.mysphera.paido

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.bson.types.ObjectId
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@Suppress("unused")
@Configuration
class JacksonConfig {
    @Bean
    fun customObjectMapper(): ObjectMapper {
        val objectIdModule =
            SimpleModule().addSerializer(ObjectId::class.java, ToStringSerializer())

        val mapper = ObjectMapper()
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        mapper.registerModules(
            listOf(
                KotlinModule(),
                objectIdModule
            )
        )
        return mapper
    }

    @Bean
    fun customMappingJackson2HttpMessageConverter(): MappingJackson2HttpMessageConverter {
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = customObjectMapper()
        return converter
    }
}
