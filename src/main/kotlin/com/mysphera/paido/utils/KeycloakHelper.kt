package com.mysphera.paido.utils

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.CredentialRepresentation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
class KeycloakHelper {
    @Value("\${microservice.keycloak.base-endpoint}")
    private lateinit var keycloakEndpoint: String

    @Value("\${microservice.keycloak.admin.realm}")
    private lateinit var keycloakAdminRealm: String

    @Value("\${microservice.keycloak.admin.client-id}")
    private lateinit var keycloakAdminClient: String

    @Value("\${microservice.keycloak.admin.user}")
    private lateinit var keycloakAdminUser: String

    @Value("\${microservice.keycloak.admin.password}")
    private lateinit var keycloakAdminPass: String

    private lateinit var client: Keycloak

    fun getClient(): Keycloak {
        if (!::client.isInitialized || client.isClosed) {
            client = KeycloakBuilder.builder()
                .serverUrl(keycloakEndpoint)
                .realm(keycloakAdminRealm).clientId(keycloakAdminClient)
                .username(keycloakAdminUser).password(keycloakAdminPass)
                .resteasyClient(ResteasyClientBuilder().connectionPoolSize(10).build())
                .build()
        }

        return client
    }

    fun newCredentialPasswordType(password: String): List<CredentialRepresentation> {
        val credentials = CredentialRepresentation()
        credentials.type = CredentialRepresentation.PASSWORD
        credentials.value = password
        return listOf(credentials)
    }
}
