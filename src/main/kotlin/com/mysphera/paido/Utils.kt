package com.mysphera.paido

import com.fasterxml.jackson.databind.ObjectMapper
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.CredentialRepresentation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.security.SecureRandom
import java.util.*
import kotlin.math.max

object FullRandomString :
    RandomString("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")

open class RandomString(characters: String) {
    private val random: Random
    private val symbols: CharArray
    private var buf: CharArray = CharArray(0)

    init {
        random = SecureRandom()
        symbols = characters.toCharArray()
    }

    /**
     * Generate a random string.
     */
    fun nextString(length: Int): String {
        if (length < 1) throw IllegalArgumentException()
        this.buf = CharArray(length)
        for (idx in buf.indices) {
            buf[idx] = symbols[random.nextInt(symbols.size)]
        }
        return String(buf)
    }
}

fun clamp(min: Int, value: Int, max: Int): Int {
    return if (value < min) {
        min
    } else {
        if (value > max) max else value
    }
}

fun getPageRequest(pageNumber: Int?, pageSize: Int?): PageRequest {
    val minPage = 0
    val maxSize = 1000

    return if (pageNumber == null || pageSize == null) {
        PageRequest.of(minPage, maxSize)
    } else {
        PageRequest.of(max(pageNumber, minPage), clamp(1, pageSize, maxSize))
    }
}

@Component
class MicroServicesToolbox(
    @Value("\${microservice.challenges.base-endpoint}")
    var challengesEndpoint: String,
    @Autowired
    var http: RestTemplate,
    @Autowired
    var jackson: ObjectMapper
)
